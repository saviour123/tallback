var json_competitor_3 = {
"type": "FeatureCollection",
"name": "competitor_3",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "ID": 0.0, "LONG": -0.1609, "LAT": 5.7022, "NAME": "Deloy Foods" }, "geometry": { "type": "Point", "coordinates": [ -0.1609, 5.7022 ] } },
{ "type": "Feature", "properties": { "ID": 1.0, "LONG": -0.2243, "LAT": 5.7009, "NAME": "Lorem Mart" }, "geometry": { "type": "Point", "coordinates": [ -0.2243, 5.7009 ] } },
{ "type": "Feature", "properties": { "ID": 2.0, "LONG": -0.2123, "LAT": 5.6609, "NAME": "Ipsum Consumables" }, "geometry": { "type": "Point", "coordinates": [ -0.2123, 5.6609 ] } },
{ "type": "Feature", "properties": { "ID": 3.0, "LONG": -0.1203, "LAT": 5.7049, "NAME": "Dosit Food" }, "geometry": { "type": "Point", "coordinates": [ -0.1203, 5.7049 ] } },
{ "type": "Feature", "properties": { "ID": 4.0, "LONG": -0.2124, "LAT": 5.6309, "NAME": "Proxy Outlet" }, "geometry": { "type": "Point", "coordinates": [ -0.2124, 5.6309 ] } },
{ "type": "Feature", "properties": { "ID": 5.0, "LONG": -0.1163, "LAT": 5.7509, "NAME": "Trent Inc" }, "geometry": { "type": "Point", "coordinates": [ -0.1163, 5.7509 ] } },
{ "type": "Feature", "properties": { "ID": 6.0, "LONG": -0.2103, "LAT": 5.6339, "NAME": "Arnod Cosmetics" }, "geometry": { "type": "Point", "coordinates": [ -0.2103, 5.6339 ] } },
{ "type": "Feature", "properties": { "ID": 7.0, "LONG": -0.2145, "LAT": 5.6879, "NAME": "Conney Food" }, "geometry": { "type": "Point", "coordinates": [ -0.2145, 5.6879 ] } },
{ "type": "Feature", "properties": { "ID": 8.0, "LONG": -0.1783, "LAT": 5.7239, "NAME": "Reset LTD" }, "geometry": { "type": "Point", "coordinates": [ -0.1783, 5.7239 ] } },
{ "type": "Feature", "properties": { "ID": 9.0, "LONG": -0.1919, "LAT": 5.6856, "NAME": "Coy Ltd" }, "geometry": { "type": "Point", "coordinates": [ -0.1919, 5.6856 ] } }
]
}
