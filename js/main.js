var map = L.map('map').setView([5.695921, -0.202467], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

let source = [];
let destination = []

geojsonOpts = {
    pointToLayer: function(feature, latlng) {
        return L.marker(latlng, {})
        .bindPopup(feature.properties.Town+'<br><b>'+feature.properties.Shop_Name+'</b>');
    }
};

var pois = L.layerGroup([
    L.geoJson(waypoints, geojsonOpts),
]).addTo(map);

var searchControl = new L.Control.Search({
    layer: pois,
    propertyName: 'Shop_Name',
    marker: false,
    zoom: 18,
    initial: false,
});


searchControl.on('search:locationfound', function(e) {

    // routing -- we route any time there is a search
    L.Routing.control({
        waypoints: [
            L.latLng(source[0].lat, source[0].lng),
            L.latLng(e.latlng.lat, e.latlng.lng)
        ],
        routeWhileDragging: true
    }).addTo(map);


    //map.removeLayer(this._markerSearch)
    if(e.layer._popup)
        e.layer.openPopup();
}).on('search:collapsed', function(e) {

    featuresLayer.eachLayer(function(layer) {	//restore feature color
        featuresLayer.resetStyle(layer);
    });	
});

map.addControl( searchControl );  //inizialize search control



// get current location
function onLocationFound(e) {
    var radius = e.accuracy / 2;

    source.push(e.latlng);

    L.marker(e.latlng).addTo(map)
        .bindPopup("You are within " + radius + " meters from this point").openPopup();
    L.circle(e.latlng, radius).addTo(map);
}

function onLocationError(e) {
    alert(e.message);
}

map.on('locationfound', onLocationFound);
map.on('locationerror', onLocationError);

map.locate({setView: true, maxZoom: 16});



    // implement hideable 
    var routingDiv = document.getElementsByClassName("leaflet-routing-alt");
    // console.log(routingDiv)
    // if(routingDiv){
    //     var span = document.createElement("BUTTON");
    //     span.className = "close";
    //     routingDiv[0].appendChild(span);
    // }

    $(".hide-route").click(function(){
        $(".leaflet-routing-alt").hide();
    });

    // var closeDiv = document.getElementsByClassName("close")
    // close.onclick = function() {
    //     var div = this.parentElement;
    //     div.style.display = "none";
    // }
